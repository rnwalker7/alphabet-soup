# Alphabet Soup

Developed by Rob Walker, for Enlighten interview process. Built on Ubuntu 22.04, using React 18.2 and Node 18.17.1, along with Yarn 3.6.1.

## How to use

Upon launching using "yarn start" or "npm start" (user preference), click on the "Upload" button to upload the input file.  Once the file is selected, it will read it in, create the search field, then locate the words, returning their locations to the screen.

Please feel free to reach out if you have any questions!