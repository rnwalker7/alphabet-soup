import './App.css';
import { useRef, useState } from "react";

function App() {  
  const fileRef = useRef(0);
  const answerRef = useRef(1);

  const [result, setResult] = useState("");
  
  var text = new Array();

  // INTERNAL FUNCTION DECLARATIONS
  
  // Read the input file, and "return" a string array
  
  function readFile(inFile) {

    let textFile = new FileReader();
    
    textFile.addEventListener("load", (e) => {
        let tmp = e.target.result.split(",");
        let fileText = atob(tmp[1]);
        text = fileText.split("\n");        
        processWordSearch();
    });
    textFile.readAsDataURL(inFile);
  }

  // Takes the file contents and puts them in the matrix...or word list

  function processWordSearch() {
    let tmpText = [...text];
    let size = tmpText[0].split("x");
    let rows = Number(size[0]);
    let searchField = new Array(rows);
    let words = [];

    let i = 1;
    let j = i + rows;

    for (i = 1; i <= rows; i++) {
      searchField.push(tmpText[i].split(" "));
    }

    let len = text.length;
    for (j = (rows + 1); j < len; j++) {
      words.push(tmpText[j].toString());
    }

    findWords(searchField, words);
  }

  // Now comes the fun part!

  function findWords(inField, inWords) {

      const searchRow = (row, word, forward)  => {
        if (row.includes(word)) {
          let idx = row.indexOf(word);
          let resBuilder = "";
          if (forward) resBuilder = word + " " + rowCount + ":" + idx + " " + rowCount + ":" + (idx + word.length - 1);
          else resBuilder = word + " " + rowCount + ":" + (row.length - idx - 1) + " " + rowCount + ":" + (row.length - (idx + word.length));
          searchResult.push(resBuilder);
        }
      };

      const searchCol = (col, word, forward)  => {
        if (col.includes(word)) {
          let idx = col.indexOf(word);
          let resBuilder = "";
          if (forward) resBuilder = word + " " + idx + ":" + colCount + " " + (idx + word.length - 1) + ":" + colCount;
          else resBuilder = word + " " + (col.length - idx - 1) + ":" + colCount + " " + (col.length - (idx + word.length)) + ":" + colCount;
          searchResult.push(resBuilder);
        }
      };

      const searchDiag1 = (text, word, forward, x, y) => {
        if (text.includes(word)) {
          let idx = text.indexOf(word);
          let resBuilder = "";
          if (forward) resBuilder = word + " " + (x + idx) + ":" + (y + idx) + " " + (x + idx + word.length - 1) + ":" + (y + idx + word.length - 1);
          else resBuilder = word + " " + (text.length - idx - 1 + x) + ":" + (text.length - idx - 1 + y) + " " + (text.length - (x + idx + word.length)) + ":" + (text.length - (y + idx + word.length));
          searchResult.push(resBuilder);
        }

      };

      const searchDiag2 = (text, word, forward, x, y) => {
        if (text.includes(word)) {
          let idx = text.indexOf(word);
          let resBuilder = "";
          if (forward) resBuilder = word + " " + (x + idx) + ":" + (y + idx) + " " + (x + idx + word.length - 1) + ":" + (y + idx + word.length - 1);
          else resBuilder = word + " " + (text.length - idx - 1 + x) + ":" + (text.length - idx - 1 + y) + " " + (text.length - (x + idx + word.length)) + ":" + (text.length - (y + idx + word.length));
          searchResult.push(resBuilder);
        }

      };

      let searchResult = [];

      // Check horizontally
      let rowCount = 0;
      let colCount = 0;
      let words = [];
      let field = [];

      inField.forEach((row) => {
        if(row !== []) field.push(row);
      });

      inWords.forEach((newWord) => {
        if (newWord !== "") words.push(newWord);
      });

      console.log(field);
      console.log(words);
  

      field.forEach((row) => {
        let tmpString = [...row].join("");
        words.forEach((word) => searchRow(tmpString, word, true));
        // Check reverse order in the row before we move to the next row
        tmpString = [...tmpString].reverse().join("");
        words.forEach((word) => searchRow(tmpString, word, false));
        rowCount++;     // Next row!
      });

      // Check vertically

      let cols = new Array(rowCount);
      
      let i = 0;
      for (i = 0; i < cols.length; i++) {
        cols[i] = [];
      }

      field.forEach((row) => {
        let x = 0;
        row.forEach((el) => { cols[x].push(el); x++ });
        
      });

      cols.forEach((col) => {
        let tmpString = [...col].join("");
        words.forEach((word) => searchCol(tmpString, word, true));
        // Check reverse order in the row before we move to the next row
        tmpString = [...tmpString].reverse().join("");
        words.forEach((word) => searchCol(tmpString, word, false));
        colCount++;     // Next column!
      });
      

      // Check diagonally

      words.forEach((word) => {
        let diagLength = word.length;
        let tmpString = "";

        // Search along X axis
        for (i = 0; i <= (field.length - diagLength); i++) {
          let j = 0;
          for (j = i; j < field.length; j++) {
            tmpString += field[j][j - i];
          }
          searchDiag1(tmpString, word, true, i, 0);

          // Now reverse!
          tmpString = [...tmpString].reverse().join("");
          searchDiag1(tmpString, word, false, i, 0);
        }

        tmpString = "";
        // Now along Y axis
        for (i = (field.length - diagLength); i > 0 ; i--) {
          let j = 0;
          for (j = 0; j < field.length; j++) {
            tmpString += field[j][i + j];
          }
          searchDiag1(tmpString, word, true, 0, i);

          // Now reverse!
          tmpString = [...tmpString].reverse().join("");
          searchDiag1(tmpString, word, false, 0, i);
        }
      });

      // Now going up, instead of down

      words.forEach((word) => {
        let diagLength = word.length;
        let maxLength = field.length;
        let tmpString = "";

        // Search along X axis
        for (i = 0; i < (maxLength - diagLength); i++) {
          let j = 0;
          for (j = 0; j < diagLength; j++) {
            tmpString += field[maxLength - i - 1 - j][j];
          }
          searchDiag2(tmpString, word, true, (maxLength - i - 1), 0);

          // Now reverse!
          tmpString = [...tmpString].reverse().join("");
          searchDiag2(tmpString, word, false, (maxLength - i - 1), 0);
        }

        tmpString = "";
        // Now along Y axis
        for (i = 0; i <= (maxLength - diagLength - 1) ; i++) {
          let j = 0;
          for (j = i; j < maxLength; j++) {
            tmpString += field[i][maxLength - j - 1 - i];
          }
          searchDiag2(tmpString, word, true, 0, maxLength - j - 1);

          // Now reverse!
          tmpString = [...tmpString].reverse().join("");
          searchDiag2(tmpString, word, false, 0, maxLength - j - 1);
        }
      });

      // Final piece, push the results to output

      let finalResult = "";

      words.forEach((word) => {
        searchResult.forEach((res) => {
          if (res.includes(word)) finalResult += res + "<br />";
        });      
      });
  
      answerRef.current.innerHTML = finalResult;

  }
  
  const uploadClick = () => {         // Fires click event handler to open file picker
    fileRef.current.click();
  };


  // Event listener for when file selected; will fire off module to read file when file selected
  
  const handleFile = (e) => {
    const fileHandler = e.target.files[0];
    let fileName = fileHandler.name;
    readFile(fileHandler);
  };
  
  return (
    <div className="App">
      <div className="card">
        <h3>Click here to upload file:</h3>
        <input type="file" onChange={handleFile} ref={fileRef} style={{display: 'none'}} accept="text/plain" />
        <button onClick={uploadClick}>Upload</button>
      </div>
      <div className="card">
        <h3>Answers:</h3>
        <div ref={answerRef}>{result}</div>
      </div>
    </div>
  );
}

export default App;
